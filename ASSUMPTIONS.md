## http://localhost/loket/php/transaction/purchase
Request:
{
	"name":"s",
	"tickets": [
		{
			"id_ticket" : "2",
			"ticket_qty" : "2"
		},
		{
			"id_ticket" : "1",
			"ticket_qty" : "10"
		}
	]
}

Response:
{"success":"Purchase ticket success","idTransaction":"3","idCustomer":"4"}{"success":"Purchase ticket success","idTransaction":"3","idCustomer":"4"}

## http://localhost/loket/php/event/ticket/create [POST]
Request:
{
  "name":"d",
  "description" : "s",
  "id_event" : "2",
  "price": 200,
  "quota": 20,
  "category": "VIP",
  "id_location" : "1",
  "start_date": "2018-01-01 10:10:10",
  "end_date": "2018-01-01 10:10:10"
}

Response:
{"success":"Insert ticket data success","lastId":"5"}

## http://localhost/loket/php/transaction/get_info [GET]
Response:
[
    {
        "transaction_id": "1",
        "transaction_date": "2018-07-11 13:47:05",
        "purchase": [
            {
                "customer": [
                    {
                        "customer_id": "2",
                        "customer_name": "s"
                    }
                ],
                "purchase_id": "2",
                "purchase_ticket_qty": "2",
                "purchase_id_transaction": "1",
                "purchase_created_at": "2018-07-11 13:47:05",
                "purchase_updated_at": "2018-07-11 13:47:05",
                "ticket": [
                    {
                        "ticket_id": "2",
                        "ticket_name": "test",
                        "ticket_desc": "test",
                        "ticket_category": "vip",
                        "ticket_price": "1000",
                        "ticket_quota": "20",
                        "ticket_created_at": "2018-07-11 08:41:12",
                        "ticket_updated_at": "2018-07-11 08:41:12"
                    }
                ]
            }
        ]
    },
    {
        "transaction_id": "2",
        "transaction_date": "2018-07-11 13:47:18",
        "purchase": [
            {
                "customer": [
                    {
                        "customer_id": "2",
                        "customer_name": "s"
                    },
                    {
                        "customer_id": "3",
                        "customer_name": "s"
                    }
                ],
                "purchase_id": "3",
                "purchase_ticket_qty": "2",
                "purchase_id_transaction": "2",
                "purchase_created_at": "2018-07-11 13:47:18",
                "purchase_updated_at": "2018-07-11 13:47:18",
                "ticket": [
                    {
                        "ticket_id": "2",
                        "ticket_name": "test",
                        "ticket_desc": "test",
                        "ticket_category": "vip",
                        "ticket_price": "1000",
                        "ticket_quota": "20",
                        "ticket_created_at": "2018-07-11 08:41:12",
                        "ticket_updated_at": "2018-07-11 08:41:12"
                    }
                ]
            },
            {
                "customer": [
                    {
                        "customer_id": "2",
                        "customer_name": "s"
                    },
                    {
                        "customer_id": "3",
                        "customer_name": "s"
                    },
                    {
                        "customer_id": "3",
                        "customer_name": "s"
                    }
                ],
                "purchase_id": "4",
                "purchase_ticket_qty": "10",
                "purchase_id_transaction": "2",
                "purchase_created_at": "2018-07-11 13:47:18",
                "purchase_updated_at": "2018-07-11 13:47:18",
                "ticket": [
                    {
                        "ticket_id": "2",
                        "ticket_name": "test",
                        "ticket_desc": "test",
                        "ticket_category": "vip",
                        "ticket_price": "1000",
                        "ticket_quota": "20",
                        "ticket_created_at": "2018-07-11 08:41:12",
                        "ticket_updated_at": "2018-07-11 08:41:12"
                    },
                    {
                        "ticket_id": "1",
                        "ticket_name": "test",
                        "ticket_desc": "test",
                        "ticket_category": "vip",
                        "ticket_price": "1000",
                        "ticket_quota": "20",
                        "ticket_created_at": "2018-07-11 08:41:12",
                        "ticket_updated_at": "2018-07-11 08:41:12"
                    }
                ]
            }
        ]
    }
]

## http://localhost/loket/php/event/get_info [GET]
Response:
[
    {
        "location_id": "1",
        "location_name": "Jakarta",
        "location_desc": "Gelora Bung Karno",
        "location_created_at": "2018-07-11 06:17:12",
        "location_updated_at": "2018-07-11 06:17:12",
        "event": [
            {
                "event_id": "2",
                "event_name": "test",
                "event_desc": "test",
                "event_start": "2018-01-01 10:10:10",
                "event_end": "2018-01-01 10:10:10",
                "event_created_at": "2018-07-11 08:03:29",
                "event_updated_at": "2018-07-11 08:03:29",
                "ticket": [
                    {
                        "ticket_id": "1",
                        "ticket_name": "test",
                        "ticket_desc": "test",
                        "ticket_category": "vip",
                        "ticket_price": "1000",
                        "ticket_quota": "20",
                        "ticket_created_at": "2018-07-11 08:41:12",
                        "ticket_updated_at": "2018-07-11 08:41:12"
                    },
                    {
                        "ticket_id": "2",
                        "ticket_name": "test",
                        "ticket_desc": "test",
                        "ticket_category": "vip",
                        "ticket_price": "1000",
                        "ticket_quota": "20",
                        "ticket_created_at": "2018-07-11 08:41:12",
                        "ticket_updated_at": "2018-07-11 08:41:12"
                    },
                    {
                        "ticket_id": "4",
                        "ticket_name": "test",
                        "ticket_desc": "test",
                        "ticket_category": "test",
                        "ticket_price": "1000",
                        "ticket_quota": "20",
                        "ticket_created_at": "2018-07-11 14:22:35",
                        "ticket_updated_at": "2018-07-11 14:22:35"
                    }
                ]
            }
        ]
    },
    {
        "location_id": "2",
        "location_name": "Bandung",
        "location_desc": "Bandung",
        "location_created_at": "2018-07-11 09:05:00",
        "location_updated_at": "2018-07-11 09:05:00",
        "event": [
            {
                "event_id": "3",
                "event_name": "test2",
                "event_desc": "test2",
                "event_start": "2018-01-01 10:10:10",
                "event_end": "2018-01-01 10:10:10",
                "event_created_at": "2018-07-11 08:03:29",
                "event_updated_at": "2018-07-11 08:03:29",
                "ticket": [
                    {
                        "ticket_id": "3",
                        "ticket_name": "test",
                        "ticket_desc": "test",
                        "ticket_category": "vip",
                        "ticket_price": "1000",
                        "ticket_quota": "20",
                        "ticket_created_at": "2018-07-11 08:41:12",
                        "ticket_updated_at": "2018-07-11 08:41:12"
                    }
                ]
            }
        ]
    },
    {
        "location_id": "3",
        "location_name": "test",
        "location_desc": "test",
        "location_created_at": "2018-07-11 09:05:31",
        "location_updated_at": "2018-07-11 09:05:31",
        "event": []
    },
    {
        "location_id": "4",
        "location_name": "test",
        "location_desc": "test",
        "location_created_at": "2018-07-11 14:25:44",
        "location_updated_at": "2018-07-11 14:25:44",
        "event": []
    }
]

## http://localhost/loket/php/location/create [POST]
Request:
{
 "name": "test",
 "description": "test",
 "id_event": 2,
 "category": "test",
 "price": 1000,
 "quota": 20
}

Response:
{"success":"Insert location data success","lastId":"4"}

## http://localhost/loket/php/event.php/ticket/create [POST]
Request:
{
 "name": "test",
 "description": "test",
 "id_event": 2,
 "category": "test",
 "price": 1000,
 "quota": 20
}

Response:
{"success":"Insert ticket data success","lastId":"4"}
