<?php
require 'helper/Api.php';
require 'helper/Dao.php';

use API\Controller\Api;
use API\Controller\ApiController;

class TransactionController extends ApiController
{
    public function __construct()
    {
        $db = Dao::getInstance();
        $this->_dbh = $db->getConnection();
        $this->_dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

/** :POST :{method} */
    public function purchase()
    {
        // main logic
        $_POST = json_decode(file_get_contents('php://input'), true);
        if( !empty($_POST) ) {
          $return = false;
        	$tickets = array(
        				  'id_ticket' => '',
                  'ticket_qty' => ''
                );

          foreach($_POST['tickets'] as $a => $b ){
            foreach($b as $k => $v ){
          		if (array_key_exists($k, $tickets)){
                $tickets[$a][$k] = trim($v);
              }
          	}

            foreach ($tickets[$a] as $key => $value) {
              if($tickets[$a][$key] == '')
              {
                 $errmsg[] = [
                   'row' => $a,
                   'msg' => 'Please enter '.$key
                 ];
              }
            }
        	}

        	if (count($errmsg)==0){
              try {
                  $dataTicket = [];
                  foreach ($_POST['tickets'] as $pos => $posVal) {
                     $id_ticket = $posVal['id_ticket'];
                     $checkIdTicket = $this->_dbh->query('SELECT id_ticket FROM ticket where id_ticket='.$id_ticket);
                     if(count($checkIdTicket->fetch()>0)){
                        $dataTicket[] = [
                          'id_ticket' => $posVal['id_ticket'],
                          'ticket_qty' => $posVal['ticket_qty']
                        ];
                     }
                  }

                  if(count($dataTicket)>0){
                    $date = new DateTime('now', new DateTimeZone('Asia/Jakarta'));
                    $rowCustomer = [
                        'name' => $_POST['name']
                    ];
                    $customer = "INSERT INTO customer (
                            name
                          ) values (
                            :name
                          )";
                    $dataCustomer = $this->_dbh->prepare($customer)->execute($rowCustomer);
                    if ($dataCustomer) {
                        $idCustomer = $this->_dbh->lastInsertId();
                        $rowTransaction = [
                            'transaction_date' => $date->format('Y-m-d H:i:s')
                        ];
                        $transaction = "INSERT INTO transaction (
                                transaction_date
                              ) values (
                                :transaction_date
                              )";
                        $dataTransaction = $this->_dbh->prepare($transaction)->execute($rowTransaction);
                        if ($dataTransaction) {
                          $idTransaction = $this->_dbh->lastInsertId();
                          foreach ($dataTicket as $dt => $dtVal) {
                            $rowPurchase = [
                                'id_customer' => $idCustomer,
                                'id_ticket' => $dtVal['id_ticket'],
                                'id_transaction' => $idTransaction,
                                'ticket_qty' => $dtVal['ticket_qty'],
                                'created_at' => $date->format('Y-m-d H:i:s'),
                                'updated_at' => $date->format('Y-m-d H:i:s')
                            ];
                            $purchase = "INSERT INTO purchase (
                                    id_customer,
                                    id_ticket,
                                    id_transaction,
                                    ticket_qty,
                                    created_at,
                                    updated_at
                                  ) values (
                                    :id_customer,
                                    :id_ticket,
                                    :id_transaction,
                                    :ticket_qty,
                                    :created_at,
                                    :updated_at
                                  )";
                            $dataPurchase = $this->_dbh->prepare($purchase)->execute($rowPurchase);
                            if ($dataTransaction) {
                              $data = [
                                'success'    => 'Purchase ticket success',
                                'idTransaction'     => $idTransaction,
                                'idCustomer' => $idCustomer
                              ];
                              echo json_encode($data);
                            }else{
                              $data = [
                                'error'    => 'Failed when insert purchase data, please contact our developers',
                                'code'     => '103',
                              ];
                              echo json_encode($data);
                            }
                          }

                        }else{
                          $data = [
                            'error'    => 'Failed when insert transaction data, please contact our developers',
                            'code'     => '103',
                          ];
                          echo json_encode($data);
                        }
                    }else{
                      $data = [
                        'error'    => 'Failed when insert customer data, please contact our developers',
                        'code'     => '103',
                      ];
                      echo json_encode($data);
                    }
                  }else{
                    $data = [
                      'error'    => 'Failed purchase, id ticket not valid or sold',
                      'code'     => '101',
                    ];
                    echo json_encode($data);
                  }
                 /*** close the database connection ***/
                 $this->_dbh = null;
             } catch (\PDOException $e) {
                 $data = [
                   'error'    => $e->getMessage(),
                   'code'     => '104',
                 ];

                 echo json_encode($data);
             }
        	}else{
            $data = [
              'error' => $errmsg,
              'code'  => '101'
            ];
            echo json_encode($data);
          }
        }else{
          $data = [
            'error' => 'Parameter not valid',
            'code'  => '102'
          ];
          echo json_encode($data);
        }
    }

/** :GET :{method} */
    public function get_info()
    {
      try {
           $data = [];
           $dataTransaction = [];
           $dataPurchase = [];
           $dataTicket = [];
           $dataCustomer = [];

           $transaction = $this->_dbh->query('SELECT * FROM transaction');
           while ($trans = $transaction->fetch()) {
             if($trans['id_transaction']!==null){
               $purchase = $this->_dbh->query("SELECT * FROM purchase WHERE id_transaction = ".$trans['id_transaction']);
               while ($p = $purchase->fetch()) {

                 $ticket = $this->_dbh->query("SELECT * FROM ticket WHERE id_ticket = ".$p['id_ticket']);
                 while ($ti = $ticket->fetch()) {
                    $dataTicket[] = [
                      "ticket_id" => $ti['id_ticket'],
                      "ticket_name" => $ti['name'],
                      "ticket_desc" =>  $ti['description'],
                      "ticket_category" => $ti['category'],
                      "ticket_price" => $ti['price'],
                      "ticket_quota" => $ti['quota'],
                      "ticket_created_at" => $ti['created_at'],
                      "ticket_updated_at" => $ti['updated_at']
                    ];
                 }

                 $customer = $this->_dbh->query("SELECT * FROM customer WHERE id_customer = ".$p['id_customer']);
                 while ($c = $customer->fetch()) {
                    $dataCustomer[] = [
                      "customer_id" => $c['id_customer'],
                      "customer_name" => $c['name']
                    ];
                 }

                 $dataPurchase[] = [
                   "customer" => $dataCustomer,
                   "purchase_id"    => $p['id_purchase'],
                   "purchase_ticket_qty"  => $p['ticket_qty'],
                   "purchase_id_transaction"  => $p['id_transaction'],
                   "purchase_created_at" => $p['created_at'],
                   "purchase_updated_at" => $p['updated_at'],
                   "ticket" => $dataTicket
                 ];
               }
             }

             $dataTransaction = [
               'transaction_id' => $trans['id_transaction'],
               'transaction_date' => $trans['transaction_date'],
               'purchase' => $dataPurchase
             ];

             $dataTicket=[];
             $dataPurchase =[];
             $data[] = $dataTransaction;
           }
           echo json_encode($data);
           /*** close the database connection ***/
           $this->_dbh = null;
       } catch (\PDOException $e) {
           echo $e->getMessage();
       }
    }

}

$api = new Api();
$api->handle();
