<?php
require 'helper/Api.php';
require 'helper/Dao.php';

use API\Controller\Api;
use API\Controller\ApiController;

class EventController extends ApiController
{
    public function __construct()
    {
        $db = Dao::getInstance();
        $this->_dbh = $db->getConnection();
        $this->_dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

/** :POST :{method} */
    public function create()
    {
        // main logic
        $_POST = json_decode(file_get_contents('php://input'), true);
        if( !empty($_POST) ) {
          $return = false;
        	$user = array(
        				  'name' => '',
        				  'description' => '',
        				  'id_location' => '',
        				  'start_date' => '',
        				  'end_date' => ''
                );

        	// apply trim for all defined form fields
        	foreach($_POST as $k => $v ){
        		if (array_key_exists($k, $user)){ $user[$k] = trim($v); }
        	}

        	// validate user input
           if($user['name'] == '')
           {
              $errmsg = 'Please enter event name';
        	    $flag['name'] = $flag_class;
           }
           else if($user['description'] == '')
           {
              $errmsg = 'Please enter event description';
        	    $flag['description'] = $flag_class;
           }
           else if($user['id_location'] == '')
           {
              $errmsg = 'Please enter id location';
        	    $flag['id_location'] = $flag_class;
           }
           else if($user['start_date'] == '')
           {
              $errmsg = 'Please enter start date';
        	    $flag['start_date'] = $flag_class;
           }
           else if (!$this->validateDate($user['start_date'])) {
              $errmsg = 'Please enter valid start date (Y-m-d H:i:s)';
              $flag['start_date'] = $flag_class;
           }
           else if($user['end_date'] == '')
           {
              $errmsg = 'Please enter end date';
        	    $flag['end_date'] = $flag_class;
           }
           else if (!$this->validateDate($user['end_date'])) {
              $errmsg = 'Please enter valid end date (Y-m-d H:i:s)';
              $flag['end_date'] = $flag_class;
           }
           else {
        	   // everything posted validates
        	   $data = $user;
        	   $return = true;
           }

        	if ($return){
              try {
                 $checkIdLocation = $this->_dbh->prepare("SELECT * FROM location WHERE id_location = :id_location LIMIT 1");
                 $checkIdLocation->execute(['id_location' => $_POST['id_location']]);

                 if($checkIdLocation->fetch()){
                     $date = new DateTime('now', new DateTimeZone('Asia/Jakarta'));
                     $row = [
                         'name' => $_POST['name'],
                         'description' => $_POST['description'],
                         'id_location' => $_POST['id_location'],
                         'start_date' => $_POST['start_date'],
                         'end_date' => $_POST['end_date'],
                         'created_at' => $date->format('Y-m-d H:i:s'),
                         'updated_at' => $date->format('Y-m-d H:i:s')
                     ];
                     $sql = "INSERT INTO event (
                             name,
                             description,
                             id_location,
                             start_date,
                             end_date,
                             created_at,
                             updated_at
                           ) values (
                             :name,
                             :description,
                             :id_location,
                             :start_date,
                             :end_date,
                             :created_at,
                             :updated_at
                           )";
                     $data = $this->_dbh->prepare($sql)->execute($row);
                     if ($data) {
                         $lastId = $this->_dbh->lastInsertId();
                         $data = [
                           'success'    => 'Insert event data success',
                           'lastId'     => $lastId
                         ];
                         echo json_encode($data);
                     }else{
                       $data = [
                         'error'    => 'Failed when insert event data, please contact our developers',
                         'code'     => '103',
                       ];
                       echo json_encode($data);
                     }
                 }else{
                   $data = [
                     'error'    => 'Id location not found',
                     'code'     => '100',
                   ];
                   echo json_encode($data);
                 }

                 /*** close the database connection ***/
                 $this->_dbh = null;
             } catch (\PDOException $e) {
                 $data = [
                   'error'    => $e->getMessage(),
                   'code'     => '104',
                 ];
                 echo json_encode($data);
             }
        	}else{
            $data = [
              'error' => $errmsg,
              'code'  => '101'
            ];
            echo json_encode($data);
          }
        }else{
          $data = [
            'error' => 'Parameter not valid',
            'code'  => '102'
          ];
          echo json_encode($data);
        }
    }

/** :GET :{method} */
    public function get_info()
    {
      try {
           /*** The SQL SELECT statement ***/
           $dataLoc = [];
           $dataEvent = [];
           $dataTicket = [];

           $location = $this->_dbh->query('SELECT * FROM location');
           // $location->execute();
           while ($loc = $location->fetch()) {
             if($loc['id_location']!==null){
               $event = $this->_dbh->query("SELECT * FROM event WHERE id_location = ".$loc['id_location']);
               while ($ev = $event->fetch()) {

                 $ticket = $this->_dbh->query("SELECT * FROM ticket WHERE id_event = ".$ev['id_event']);
                 while ($ti = $ticket->fetch()) {
                    $dataTicket[] = [
                      "ticket_id" => $ti['id_ticket'],
                      "ticket_name" => $ti['name'],
                      "ticket_desc" =>  $ti['description'],
                      "ticket_category" => $ti['category'],
                      "ticket_price" => $ti['price'],
                      "ticket_quota" => $ti['quota'],
                      "ticket_created_at" => $ti['created_at'],
                      "ticket_updated_at" => $ti['updated_at']
                    ];
                 }

                 $dataEvent[] = [
                   "event_id"    => $ev['id_event'],
                   "event_name"  => $ev['name'],
                   "event_desc"  => $ev['description'],
                   "event_start" => $ev['start_date'],
                   "event_end"   => $ev['end_date'],
                   "event_created_at" => $ev['created_at'],
                   "event_updated_at" => $ev['updated_at'],
                   "ticket" => $dataTicket
                 ];
               }
             }


             $dataLoc = [
               'location_id' => $loc['id_location'],
               'location_name' => $loc['name'],
               'location_desc' => $loc['description'],
               'location_created_at' => $loc['created_at'],
               'location_updated_at' => $loc['updated_at'],
               'event' => $dataEvent
             ];

             $dataTicket=[];
             $dataEvent =[];
             $data[] = $dataLoc;
           }
           echo json_encode($data);
           /*** close the database connection ***/
           $this->_dbh = null;
       } catch (\PDOException $e) {
           echo $e->getMessage();
       }
    }

/** :POST :{method}/{$action} */
    public function ticket($action)
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        if( !empty($_POST) ) {
            switch ($action) {
            case 'create':
                  $return = false;
                  $user = array(
                          'id_event' => '',
                          'name' => '',
                          'description' => '',
                          'category' => '',
                          'price' => '',
                          'quota' => ''
                        );

                  // apply trim for all defined form fields
                  foreach($_POST as $k => $v ){
                    if (array_key_exists($k, $user)){ $user[$k] = trim($v); }
                  }

                  // validate user input
                   if($user['name'] == '')
                   {
                      $errmsg = 'Please enter event name';
                      $flag['name'] = $flag_class;
                   }
                   else if($user['description'] == '')
                   {
                      $errmsg = 'Please enter event description';
                      $flag['description'] = $flag_class;
                   }
                   else if($user['id_event'] == '')
                   {
                      $errmsg = 'Please enter id event';
                      $flag['id_event'] = $flag_class;
                   }
                   else if($user['price'] == '')
                   {
                      $errmsg = 'Please enter ticket price';
                      $flag['start_date'] = $flag_class;
                   }
                   else if(!$this->isPositiveInteger($user['price']))
                   {
                      $errmsg = 'Ticket price must be positive integer';
                      $flag['price'] = $flag_class;
                   }
                   else if($user['quota'] == '')
                   {
                      $errmsg = 'Please enter ticket quota';
                      $flag['quota'] = $flag_class;
                   }
                   else if(!$this->isPositiveInteger($user['quota']))
                   {
                      $errmsg = 'Ticket quota must be positive integer';
                      $flag['quota'] = $flag_class;
                   }
                   else {
                     // everything posted validates
                     $data = $user;
                     $return = true;
                   }

                  if ($return){
                      try {
                         $checkIdLocation = $this->_dbh->prepare("SELECT * FROM event WHERE id_event = :id_event LIMIT 1");
                         $checkIdLocation->execute(['id_event' => $_POST['id_event']]);

                         if($checkIdLocation->fetch()){
                             $date = new DateTime('now', new DateTimeZone('Asia/Jakarta'));
                             $row = [
                                 'name' => $_POST['name'],
                                 'description' => $_POST['description'],
                                 'id_event' => $_POST['id_event'],
                                 'category' => $_POST['category'],
                                 'price' => $_POST['price'],
                                 'quota' => $_POST['quota'],
                                 'created_at' => $date->format('Y-m-d H:i:s'),
                                 'updated_at' => $date->format('Y-m-d H:i:s')
                             ];
                             $sql = "INSERT INTO ticket (
                                     name,
                                     description,
                                     id_event,
                                     category,
                                     price,
                                     quota,
                                     created_at,
                                     updated_at
                                   ) values (
                                     :name,
                                     :description,
                                     :id_event,
                                     :category,
                                     :price,
                                     :quota,
                                     :created_at,
                                     :updated_at
                                   )";
                             $data = $this->_dbh->prepare($sql)->execute($row);
                             if ($data) {
                                 $lastId = $this->_dbh->lastInsertId();
                                 $data = [
                                   'success'    => 'Insert ticket data success',
                                   'lastId'     => $lastId
                                 ];
                                 echo json_encode($data);
                             }else{
                               $data = [
                                 'error'    => 'Failed when insert ticket data, please contact our developers',
                                 'code'     => '103',
                               ];
                               echo json_encode($data);
                             }
                         }else{
                           $data = [
                             'error'    => 'Id event not found',
                             'code'     => '100',
                           ];
                           echo json_encode($data);
                         }

                         /*** close the database connection ***/
                         $this->_dbh = null;
                     } catch (\PDOException $e) {
                         $data = [
                           'error'    => $e->getMessage(),
                           'code'     => '104',
                         ];
                         echo json_encode($data);
                     }
                  }else{
                    $data = [
                      'error' => $errmsg,
                      'code'  => '101'
                    ];
                    echo json_encode($data);
                  }
              break;

            default:
              echo 'x';
              break;
          }
        }else{
          $data = [
            'error' => 'Parameter not valid',
            'code'  => '102'
          ];
          echo json_encode($data);
        }
    }

    private function validateDate($date){
      $format = 'Y-m-d H:i:s';
      $d = DateTime::createFromFormat($format, $date);
      return $d && $d->format($format) == $date;
    }

    private function isPositiveInteger($str) {
      return (is_numeric($str) && $str > 0 && $str == round($str));
    }
}

$api = new Api();
$api->handle();
