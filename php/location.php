<?php
require 'helper/Api.php';
require 'helper/Dao.php';

use API\Controller\Api;
use API\Controller\ApiController;

class LocationController extends ApiController
{
    public function __construct()
    {
        $db = Dao::getInstance();
        $this->_dbh = $db->getConnection();
        $this->_dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

/** :POST :{method} */
    public function create()
    {
        // main logic
        $_POST = json_decode(file_get_contents('php://input'), true);
        if( !empty($_POST) ) {
          $return = false;
        	$user = array(
        				  'name' => '',
        				  'description' => ''
                );

        	// apply trim for all defined form fields
        	foreach($_POST as $k => $v ){
        		if (array_key_exists($k, $user)){ $user[$k] = trim($v); }
        	}

        	// validate user input
           if($user['name'] == '')
           {
              $errmsg = 'Please enter location name';
        	    $flag['name'] = $flag_class;
           }
           else if($user['description'] == '')
           {
              $errmsg = 'Please enter location description';
        	    $flag['description'] = $flag_class;
           }
           else {
        	   // everything posted validates
        	   $data = $user;
        	   $return = true;
           }

        	if ($return){
              try {
                 $date = new DateTime('now', new DateTimeZone('Asia/Jakarta'));
                 $row = [
                     'name' => $_POST['name'],
                     'description' => $_POST['description'],
                     'created_at' => $date->format('Y-m-d H:i:s'),
                     'updated_at' => $date->format('Y-m-d H:i:s')
                 ];
                 $sql = "INSERT INTO location (
                         name,
                         description,
                         created_at,
                         updated_at
                       ) values (
                         :name,
                         :description,
                         :created_at,
                         :updated_at
                       )";
                 $data = $this->_dbh->prepare($sql)->execute($row);
                 if ($data) {
                     $lastId = $this->_dbh->lastInsertId();
                     $data = [
                       'success'    => 'Insert location data success',
                       'lastId'     => $lastId
                     ];
                     echo json_encode($data);
                 }else{
                   $data = [
                     'error'    => 'Failed when insert location data, please contact our developers',
                     'code'     => '103',
                   ];
                   echo json_encode($data);
                 }
                 /*** close the database connection ***/
                 $this->_dbh = null;
             } catch (\PDOException $e) {
                 $data = [
                   'error'    => $e->getMessage(),
                   'code'     => '104',
                 ];
                 echo json_encode($data);
             }
        	}else{
            $data = [
              'error' => $errmsg,
              'code'  => '101'
            ];
            echo json_encode($data);
          }
        }else{
          $data = [
            'error' => 'Parameter not valid',
            'code'  => '102'
          ];
          echo json_encode($data);
        }
    }
}

$api = new Api();
$api->handle();
